function showLoading() {
    $('#cover-spin').show();
}

function hideLoading() {
    $('#cover-spin').hide();
}

function loadImage(keyword, resultObj) {
    var apiurl,
        thumbnail_size,
        json_last_call;

    var ajax1Completed = {};
    apiurl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&tags=" + keyword + "&&safe_search=1&api_key=b7fbe027a3570dc156761878491e523d&per_page=20&format=json&nojsoncallback=1&sort=relevance";
    thumbnail_size = 500;
    showLoading();
    $.getJSON(apiurl, function (json) { }).done(function (json) {
        $.each(json.photos.photo, function (i, myresult) {
            apiurl_size = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=b7fbe027a3570dc156761878491e523d&photo_id=" + myresult.id + "&format=json&nojsoncallback=1";
            $.getJSON(apiurl_size, function (size) {
                ajax1Completed[myresult.id] = false;
                $.each(size.sizes.size, function (i, myresult_size) {
                    if (myresult_size.width >= thumbnail_size) {
                        var temp_result = '<div class="col-lg-3 col-md-4 col-6">';
                        temp_result += '<a data-image-id="' + myresult.id + '" data-title="' + myresult.title + '" href="#" class="result-img d-block mb-4 h-100">';
                        temp_result += '<img style="display: block;overflow: hidden;"  class="img-flickr-results img-fluid img-thumbnail" src="' + myresult_size.source + '" alt="">';
                        temp_result += '</a>';
                        temp_result += '</div>';
                        resultObj.append(temp_result);
                        return false;

                    }
                })
            }).done(function () { // if it's last image
                if (Object.keys(ajax1Completed).length == json.photos.photo.length) {
                    $('.result-img').click(function () {
                        showImageDetails($(this).data('image-id'))
                        $('#image-details').show();
                        $('#results').hide();
                    });
                    hideLoading();
                }
            });
        });
    });
}

function showImageDetails(image_id) {
    image_size = 600;
    apiurl_size = "https://api.flickr.com/services/rest/?method=flickr.photos.getSizes&api_key=b7fbe027a3570dc156761878491e523d&photo_id=" + image_id + "&format=json&nojsoncallback=1";
    console.log(apiurl_size);
    showLoading();
    $('#image-details').html('');
    $.getJSON(apiurl_size, function (size) {
        var previousWidth = 0;
        var largestImageRow,
            imageLabel;
        $.each(size.sizes.size, function (i, myresult_size) {
            if (previousWidth < myresult_size.width) {
                largestImageRow = myresult_size;
                previousWidth = myresult_size.width;
            }
        })

        var searchText = largestImageRow.label;
        if (previousWidth > 0) {
            var temp_result = '<div class="col-lg-8 col-md-8 col-12">';
            temp_result += '<img style="display: block;overflow: hidden;" image-id="+myresult.id+" class="img-fluid img-thumbnail" src="' + largestImageRow.source + '" alt="">';
            temp_result += '</div>';
            temp_result += '<div class="col-lg-4 col-md-4 col-12">';
            temp_result += '<div class="row">';
            temp_result += '<div class="col-7">';
            temp_result += '<h2>Image details</h2>';
            temp_result += '</div>';
            temp_result += '<div class="col-5">';
            temp_result += '<button id="image-details-back-btn" class="btn btn-secondary ml-auto">Back</button>';
            temp_result += '</div></div>';
            temp_result += '<div class="row"><div class="col-12">';
            temp_result += '<p>' + $('a[data-image-id="' + image_id + '"]').data('title') + '</p>';
            temp_result += '</div></div></div>';
            temp_result += '</div>';
            $('#image-details').html(temp_result);

            $('#image-details-back-btn').click(function () {
                $('#image-details').hide();
                $('#results').show();
            });
            hideLoading();

            return false;
        }
    })
}

jQuery(function ($) {
    if ($(".jquery-category-list").length) {
        $('#top_header').html('Gallery Category');
        $('.category-nav-btn').click(function () {
            $('.category-nav-btn').removeClass('active');
            $(this).addClass('active');
            $('#results').html('');
            $('#top_header').html('Gallery ' + $(this).text());
            loadImage($(this).text(), $('#results'));
            $('#image-details').hide();
            $('#results').show();
        });


        // Initialise the category list
        $('.category-nav-btn').first().addClass('active');
        loadImage($('.category-nav-btn').first().text(), $('#results'));
    }

    if ($(".symfony-category-list").length) {
        setTimeout(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: '.grid-sizer',
                percentPosition: true,
            });

            $('.grid').addClass("active");
        }, 100);

        $('#category-content').magnificPopup({
            delegate: '.card',
            enabled: true,
            type: 'ajax',
        });

        $("#category-panel .list-group .list-group-item").click(showLoading);
    }
});
