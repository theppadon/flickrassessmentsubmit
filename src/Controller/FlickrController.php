<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Category;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class FlickrController extends AbstractController
{
    /**
     * @Route("/flickr", name="flickr")
     */
    public function index(EntityManagerInterface $entityManager)
    {
        $categories = $entityManager->getRepository(Category::class)->findBy([], ['name' => 'asc']);

        return $this->render('flickr/index.html.twig', [
            'controller_name' => 'FlickrController',
            'categories' => $categories,
        ]);
    }
}
