<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpClient\HttpClient;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Category;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class FlickrNewController extends AbstractController
{
    /**
     * @Route("/flickr-new/{keyword}", name="flickr_new")
     */
    public function index(EntityManagerInterface $entityManager, $keyword = null)
    {
        $categories = $entityManager->getRepository(Category::class)->findBy([], ['name' => 'asc']);

        // if no category in url, then try to get one from db
        if (!$keyword) {
            // gets category, so redirect this page to implement that category
            if (!empty($categories)) return $this->redirectToRoute("flickr_new", [
                "keyword" => $categories[0]->getKeyword(),
            ]);

            // no category in db, means should display images without category
            else {
                $page_title = "Recent Images";
                $additional_query = [
                    "method" => "flickr.photos.getRecent",
                ];
            }
        }

        // there is category in url, display images based on it
        else {
            $category_name = null;

            // get category name by matching category in url and category in db
            foreach ($categories as $category) {
                if ($category->getKeyword() == $keyword) {
                    $category_name = $category->getName();
                    break;
                }
            }

            // but user can also put any category directly from url
            // in this case, that category may not be found on db
            // hence the category name will be still null, and should fill it manually
            if (empty($category_name)) {
                $category_name = $keyword;
                $category_name = str_replace("_", " ", $category_name);
                $category_name = str_replace("-", " ", $category_name);
                $category_name = ucwords($category_name);
            }

            $page_title = $category_name . " Images";
            $additional_query = [
                "method" => "flickr.photos.search",
                "tags"   => $keyword,
            ];
        }

        // ============================================================================== start gathering photo data from flickr
        $api_key = $this->getParameter("flickr_api_key");
        $max_list = $this->getParameter("flickr_max_list");
        $api_url = "https://api.flickr.com/services/rest/";

        $url_queries = $additional_query + [
            "api_key"        => $api_key,
            "per_page"       => $max_list,
            "safe_search"    => "1",
            "sort"           => "relevance",
            "format"         => "json",
            "nojsoncallback" => "1",
        ];

        $photo_data = HttpClient::create()->request("GET", $api_url . "?" . http_build_query($url_queries))->getContent();
        $photo_data = json_decode($photo_data, true);

        // ============================================================================== now gathering size data from flickr
        $photos = [];
        foreach ($photo_data["photos"]["photo"] as $photo) {
            $photos[] = [
                "id"     => $photo["id"],
                "title"  => $photo["title"],
                "server"  => $photo["server"],
                "secret"  => $photo["secret"],
                "small" => "https://live.staticflickr.com/" . $photo["server"] . "/" . $photo["id"] . "_" . $photo["secret"] . "_m.jpg",
                "big" => "https://live.staticflickr.com/" . $photo["server"] . "/" . $photo["id"] . "_" . $photo["secret"] . ".jpg",
            ];
        }

        return $this->render('flickr/list.html.twig', [
            "page_title" => $page_title,
            "category_name" => $category_name,
            "categories" => $categories,
            "photos" => $photos,
        ]);
    }

    /**
     * @Route("/flickr-details-new/{photo_server}/{photo_id}/{photo_secret}", name="flickr_details_new")
     */
    public function details(EntityManagerInterface $entityManager, $photo_server, $photo_id, $photo_secret)
    {
        $api_key = $this->getParameter("flickr_api_key");
        $api_url = "https://api.flickr.com/services/rest/";

        $url_queries = [
            "method"         => "flickr.photos.getInfo",
            "api_key"        => $api_key,
            "photo_id"       => $photo_id,
            "format"         => "json",
            "nojsoncallback" => "1",
        ];

        $photo = HttpClient::create()->request("GET", $api_url . "?" . http_build_query($url_queries))->getContent();
        $photo = json_decode($photo, true);

        // add compiled tags
        $temp = [];
        foreach ($photo["photo"]["tags"]["tag"] as $tag) $temp[] = $tag["_content"];
        $photo["photo"]["tags_compiled"] = implode(", ", $temp);

        return $this->render('flickr/details.html.twig', [
            "photo" => $photo["photo"],

            "photo_server" => $photo_server,
            "photo_id" => $photo_id,
            "photo_secret" => $photo_secret,
        ]);
    }
}
