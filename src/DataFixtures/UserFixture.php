<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
		
        $user = new User();
        $user->setEmail('admin_user@test.com');
        $user->setPassword('12345');
        $user->setRoles('ROLE_ADMIN');
        
        $manager->presist(user);


        $manager->flush();
    }
}
